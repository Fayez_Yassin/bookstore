﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApplication11.model;
using WebApplication11.model.Repository;

namespace WebApplication11.controller
{
    public class AutherController : Controller
    {
        private readonly IBookstorRepostory<Auther> autherRepository;

        public AutherController(IBookstorRepostory<Auther> autherRepository)
        {
            this.autherRepository = autherRepository;
        }

        // GET: Auther
        public ActionResult Index()
        {
            var Auther = autherRepository.List();

            return View(Auther);
        }

        // GET: Auther/Details/5
        public ActionResult Details(int id)
        {
            var auther = autherRepository.Find(id);
            return View(auther);
        }

        // GET: Auther/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Auther/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Auther auther)
        {
            try
            {
                autherRepository.Add(auther);

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Auther/Edit/5
        public ActionResult Edit(int id)
        {
            var auther = autherRepository.Find(id);
            return View(auther);
        }

        // POST: Auther/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, Auther auther)
        {
            try
            {
                autherRepository.Updat(auther);

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Auther/Delete/5
        public ActionResult Delete(int id)
        {
            var auther = autherRepository.Find(id);
            return View(auther);
        }

        // POST: Auther/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, Auther auther)
        {
            try
            {
                autherRepository.Delete(auther.id);

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}