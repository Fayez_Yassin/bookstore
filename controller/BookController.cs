﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApplication11.model;
using WebApplication11.model.Repository;
using WebApplication11.viewmodel;

namespace WebApplication11.controller
{
    public class BookController : Controller
    {
        private readonly IBookstorRepostory<Book> boockstorRepostory;
        private readonly IBookstorRepostory<Auther> autherRepostory;

        public BookController(IBookstorRepostory<Book> boockstorRepostory, IBookstorRepostory<Auther> autherRepostory)
        {
            this.boockstorRepostory = boockstorRepostory;
            this.autherRepostory = autherRepostory;
        }
        // GET: Boock
        public ActionResult Index()
        {
            var boocks = boockstorRepostory.List();
            return View(boocks);
        }

        // GET: Boock/Details/5
        public ActionResult Details(int id)
        {
            var boock = boockstorRepostory.Find(id);

            return View(boock);
        }

        // GET: Boock/Create
        public ActionResult Create()
        {
            var moselview = new BoockAuthermodel
            {
                authers= autherRepostory.List().ToList()
        };
            return View(moselview);
        }

        // POST: Boock/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(BoockAuthermodel boockAuthermodel)
        {
            try
            {
                var autherfromview = autherRepostory.Find(boockAuthermodel.idauther);
                Book boock = new Book { 
                    id=boockAuthermodel.id,
                    title=boockAuthermodel.title,
                    descrption=boockAuthermodel.descrption,
                    auther= autherfromview
                };
                boockstorRepostory.Add(boock);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Boock/Edit/5
        public ActionResult Edit(int id)
        {
            var boockmdel = boockstorRepostory.Find(id);
            var moselview = new BoockAuthermodel
            {
                id=boockmdel.id,
                descrption=boockmdel.descrption,
                title=boockmdel.title,
                authers = autherRepostory.List().ToList()
            };

            return View(moselview);
        }

        // POST: Boock/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(BoockAuthermodel boockAuthermodel)
        {
            try
            {
                var autheredit = autherRepostory.Find(boockAuthermodel.idauther);
               var boockedit = new Book{
                id=boockAuthermodel.id,
                title=boockAuthermodel.title,
                descrption=boockAuthermodel.descrption,
                auther=autheredit
               };
                boockstorRepostory.Updat(boockedit);

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Boock/Delete/5
        public ActionResult Delete(int id)
        {
            var boockdelete = boockstorRepostory.Find(id);
           
                return View(boockdelete);
         
        }

        // POST: Boock/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ConfirmDelete(int id)
        {
            try
            {
                boockstorRepostory.Delete(id);

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        public ActionResult Search(String term)
        {
            if (term==null)
            {
                return View("Index",boockstorRepostory.List());
            }
            var result = boockstorRepostory.search(term);

            return View("Index",result);
        }
    }
}