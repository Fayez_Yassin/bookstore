﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication11.model;

namespace WebApplication11.viewmodel
{
    public class BoockAuthermodel
    {
        public int id { get; set; }
        
        public String title { get; set; }

        public String descrption { get; set; }

        public int idauther { get; set; }

        public List<Auther> authers { get; set; }
    }
}
