﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication11.model.Repository
{
    public class BookDbRepositpry : IBookstorRepostory<Book>
    {
        public  BookStorDbcontext db;


        // ctor
        public BookDbRepositpry(BookStorDbcontext _db )
        {
        
            db = _db;
        }
        public void Add(Book entitiy)
        {
            db.Boocks.Add(entitiy);
            db.SaveChanges();

        }

        public void Delete(int id)
        {
            var boock = Find(id);
            db.Boocks.Remove(boock);
            db.SaveChanges();
        }

        public Book Find(int id)
        {
            var boock = db.Boocks.Include(a=>a.auther).SingleOrDefault(b => b.id == id);

            return boock;
        }

        public IList<Book> List()
        {
            return db.Boocks.Include(a=>a.auther).ToList();
        }

        public List<Book> search(string term)
        {
            var result = db.Boocks.Include(a => a.auther).Where(b => b.title.Contains(term)).ToList();
            return result;
        }

        public void Updat(Book entitiy)
        {
            db.Update(entitiy);
            db.SaveChanges();

        }
    
    
    }
}
