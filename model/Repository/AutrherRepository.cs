﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication11.model.Repository
{
    public class AutrherRepository : IBookstorRepostory<Auther>
    {
        List<Auther> Authers;
        public AutrherRepository()
        {
            Authers = new List<Auther>
                {
                   new Auther(){id=1,fullname="-----plyes chose auther-----"},
                    new Auther(){id=2,fullname="fyez yassin"},
                    new Auther(){id=3,fullname="yassin yassin"},
                    new Auther(){id=4,fullname="adnan yassin"},
                };
        }
        public void Add(Auther entitiy)
        {
            Authers.Add(entitiy);
        }

        public void Delete(int id)
        {
            var auther =Find(id);
            Authers.Remove(auther);
         }

        public Auther Find(int id)
        {
            var auther = Authers.SingleOrDefault(a => a.id == id);
            return auther;
        }

        public IList<Auther> List()
        {
            return Authers;
        }

        public List<Auther> search(string term)
        {
            var res = Authers.Where(a => a.fullname.Contains(term)).ToList();
            return res;

        }

        public void Updat(Auther entitiy)
        {
            var auther = Find(entitiy.id);
            auther.fullname = entitiy.fullname;
        }
    }
}
