﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication11.model.Repository
{
    public class BookRepository : IBookstorRepostory<Book>
    {
        List<Book> Boocks;

        // ctor
        public  BookRepository()
        {
            Boocks = new List<Book>()
            {
           new Book{id=1, title="java boock", descrption="no desc" },
           new Book{id=2, title="C# boock", descrption="no desc" },
           new Book{id=3, title="php boock", descrption="no desc" },
            };
        }
        public void Add(Book entitiy)
        {
            Boocks.Add(entitiy);

        }

        public void Delete(int id)
        {
            var boock = Find(id);
            Boocks.Remove(boock);
        }

        public Book Find(int id)
        {
            var boock = Boocks.SingleOrDefault(b=> b.id == id);

            return boock;
        }

        public IList<Book> List()
        {
            return Boocks;
        }

        public List<Book> search(string term)
        {
            var res = Boocks.Where(b => b.title.Contains(term)).ToList();
            return res;
        }

        public void Updat(Book entitiy)
        {
            var boock = Find(entitiy.id);
            boock.title = entitiy.title;
            boock.descrption = entitiy.descrption;
            boock.auther = entitiy.auther;

        }
    }
}
