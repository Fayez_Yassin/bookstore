﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication11.model.Repository
{
    public class AutherDbRepository : IBookstorRepostory<Auther>
    {
        public  BookStorDbcontext db;
   
        public AutherDbRepository(BookStorDbcontext _db)
        {
         
            db = _db;
        }
        public void Add(Auther entitiy)
        {
            db.Authers.Add(entitiy);
            db.SaveChanges();
        }

        public void Delete(int id)
        {
            var auther = Find(id);
            db.Authers.Remove(auther);
            db.SaveChanges();
        }

        public Auther Find(int id)
        {
            var auther = db.Authers.SingleOrDefault(a => a.id == id);
            return auther;
        }

        public IList<Auther> List()
        {
            return db.Authers.ToList();
        }

        public List<Auther> search(string term)
        {
          return  db.Authers.Where(a => a.fullname.Contains(term)).ToList();
            
        }

        public void Updat(Auther entitiy)
        {
            db.Update(entitiy);
            db.SaveChanges();
        }
    
}
   
}
