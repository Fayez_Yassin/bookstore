﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication11.model.Repository
{
     public interface IBookstorRepostory<TEntitiy>
    {
        IList<TEntitiy> List();

        TEntitiy Find(int id);

        void Add(TEntitiy entitiy);

        void Updat(TEntitiy entitiy);

        void Delete(int id);

        List<TEntitiy> search(String term);

    }
}
